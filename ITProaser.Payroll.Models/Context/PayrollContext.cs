﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;

namespace ITProaser.Models.Context;

public sealed class PayrollContext : IdentityDbContext
{
    public PayrollContext(DbContextOptions options) : base(options) { }
}