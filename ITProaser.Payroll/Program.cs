using Microsoft.AspNetCore.DataProtection;
using Microsoft.EntityFrameworkCore;
using Microsoft.Net.Http.Headers;

var builder = WebApplication.CreateBuilder(args);
string? connectionString = builder.Configuration.GetConnectionString("DefaultConnection");
string? provider = builder.Configuration.GetValue<string>("Provider");

var localizationOptions = new Action<RequestLocalizationOptions>(options =>
{
    var supportedCultures = new List<CultureInfo> { new("en"), new("es") };
    options.SetDefaultCulture("en");
    options.ApplyCurrentCultureToResponseHeaders = true;
    options.SupportedCultures = supportedCultures;
    options.SupportedUICultures = supportedCultures;
});

// Localization service
builder.Services.AddRequestLocalization(localizationOptions).AddLocalization(options => options.ResourcesPath = "Resources");

// DbContext
builder.Services.AddDbContext<PayrollContext>(options => _ = provider switch
    {
        "SqlServer" => options.UseSqlServer(connectionString, sql => sql.MigrationsAssembly("ITProaser.Payroll.Database.SqlServer")),
        "Postgres" => options.UseNpgsql(connectionString, pg => pg.MigrationsAssembly("ITProaser.Payroll.Database.Postgres")),
        _ => throw new NotSupportedException($"Database engine {provider} is not supported.")
    }
);
builder.Services.AddDataProtection().SetApplicationName("ITProaser");

builder.Services.AddCors(cors => cors.AddDefaultPolicy(policy
    => policy.WithMethods(HttpMethods.Get, HttpMethods.Post, HttpMethods.Put, HttpMethods.Delete)
        .WithHeaders(HeaderNames.AcceptLanguage).AllowCredentials()));

builder.Services.AddResponseCompression().AddResponseCaching();
// Add services to the container.
builder.Services.AddControllers();

builder.Services.AddSpaStaticFiles(spa => spa.RootPath = "Client/dist");

var app = builder.Build();

// Configure the HTTP request pipeline.
bool isDevelopment = app.Environment.IsDevelopment();

app.UseRequestLocalization(localizationOptions);

if (isDevelopment)
    app.UseDeveloperExceptionPage();
else
    // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
    app.UseHsts();

app.UseHttpsRedirection();
app.UseStaticFiles();
app.UseSpaStaticFiles();
app.UseResponseCaching();
app.UseResponseCompression();
app.UseAuthentication();

app.UseRouting();

app.UseAuthorization();
app.UseCors();
app.MapControllers();

app.UseSpa(spa =>
{
    (spa.Options.SourcePath, spa.Options.DevServerPort) = ("Client", 3291);
    if (isDevelopment) spa.UseProxyToSpaDevelopmentServer("http://localhost:3291");
});

app.Run();